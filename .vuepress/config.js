module.exports = {
  "title": "Rohan Sakhale",
  "description": "Technology Evangelist, Product Engineer, Analytics Expert",
  "dest": "dist",
  "head": [
    ["link", { "rel": "icon", "href": "/favicon.ico" }],
    ["meta", { "name": "viewport", "content": "width=device-width,initial-scale=1,user-scalable=no" }],
    ["meta", { "name": "og:title", "content": "Rohan Sakhale - Technology Evangelist, Product Engineer, Analytics Expert" }],
    ["meta", { "name": "og:description", "content": "Known as Software Mentor by passion and Engineer by expertise with over 12+years in Web Development as Freelancer & 9+ years of Industrial Development experience. Thanks for several dimension from Prof. Rajesh Patkar (my Guru) his perception has always help me perceive from idea to product in effective, efficient & cost saving manner." }],
    ["meta", { "name": "og:type", "content": "blog" }],
    ["meta", { "name": "og:image", "content": "https://rohansakhale.com/favicon.png" }]
  ],
  "theme": "reco",
  "themeConfig": {
    repo: "https://gitlab.com/rsakhale/rsakhale.gitlab.io",
    editLink: true,
    editLinkText: 'Help us improve this page!',
    hostname: "rohansakhale.com",
    subSidebar: true,
    noFoundPageByTencent: false,
    "nav": [
      { "text": "Home", "link": "/", "icon": "reco-home" },
      { "text": "TimeLine", "link": "/timeline/", "icon": "reco-date" },
      { "text": "About", "icon": "reco-account", "link": "/about/" },
      {
        "text": "Contact",
        "icon": "reco-message",
        "items": [
          {
            "text": "GitLab",
            "link": "https://gitlab.com/rsakhale",
            "icon": "reco-gitlab"
          },
          {
            "text": "GitHub",
            "link": "https://github.com/RohanSakhale",
            "icon": "reco-github"
          },
          {
            "text": "Twitter",
            "link": "https://twitter.com/RohanSakhale",
            "icon": "reco-twitter"
          },
          {
            "text": "LinkedIn",
            "link": "https://linkedin.com/in/rsakhale",
            "icon": "reco-linkedin"
          }
        ]
      }
    ],
    "sidebar": {

    },
    "type": "blog",
    "blogConfig": {
      "category": {
        "location": 2,
        "text": "Category"
      },
      "tag": {
        "location": 3,
        "text": "Tag"
      }
    },
    "friendLink": [
      {
        "title": "Sai Ashirwad Informatia",
        "desc": "Empowering your business with technology",
        "email": "sales@saiashirwad.com",
        "link": "https://saiashirwad.com"
      },
      {
        "title": "Send SMS",
        "desc": "A simple API to send SMS anywhere in the world",
        "email": "sales@saiashirwad.com",
        "link": "https://saiashirwad.in"
      },
      {
        "title": "Professional Web",
        "desc": "Buy domain or cloud hosting with confidence & expert support",
        "email": "sales@saiashirwad.com",
        "link": "https://saiashirwad.pw"
      },
    ],
    "logo": "/avatar.jpg",
    "search": true,
    "searchMaxSuggestions": 10,
    "lastUpdated": "Last Updated",
    "author": "Rohan Sakhale",
    "authorAvatar": "/avatar.jpg",
    "record": "",
    "startYear": "2011"
  },
  "markdown": {
    "lineNumbers": true
  },
  plugins: [
    [
      '@vuepress/google-analytics',
      {
        'ga': 'UA-11776438-12'
      }
    ],
    ['redirect-frontmatter', {}],
    [
      "@mr-hope/sitemap",
      {
        hostname: "https://rohansakhale.com"
      },
    ],
    [
      '@vuepress-reco/vuepress-plugin-rss',
      {
        site_url: 'https://rohansakhale.com',
        copyright: 'Rohan Sakhale'
      }
    ],
    ['@vuepress/last-updated', {}],
    ['@vuepress/nprogress', {}],
    ['seo', {}],
    ['vuepress-plugin-reading-time', {}],
    ['reading-progress', {}],
    ['disqus', { shortname: 'rohansakhale' }]
  ]
}