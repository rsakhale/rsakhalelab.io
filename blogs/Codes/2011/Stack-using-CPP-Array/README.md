---
title: Stack using CPP Array
date: 2011-11-10 16:26:36
categories:
  - Codes
tags:
  - code-example
  - cpp
redirectFrom: /2011/11/10/Stack-using-CPP-Array/
---
### Summary

A stack is an order of list in which all insertion and deletion are made at one end.

The below is the code representing demonstration of working of stack by using push() and pop() functions for entering and removing elements from stack respectively.

<!-- more -->
### Code

```cpp
#include "stdio.h"
#include "conio.h"

void push();
void pop();
void display();
int a[6],top=0;

void main()
{
	int i,ch;
	clrscr();
	printf("Enter the following to do the operation:=> \n");
	printf("1. Create\n2. Push\n3. Pop\n4. Display\n5. Exit");
	do
	{
		printf("Enter your choice:=> ");
		scanf("%d",&ch);
		switch(ch)
		{
			case 1:
				printf("New Stack is Created");
				top = 0;
				break;
			case 2:
				push();
				break;
			case 3:
				pop();
				break;
			case 4:
				display();
				break;
			case 5:
				i = 1;
				break;
			default:
				printf("Wrong Choice Entered");
		}
	}while(i!=1);
	getch();
}

// Function to push values / elements in stack

void push()
{
	int n;
	if(top>5)
	{
		printf("Stack is Full");
	}
	else
	{
		printf("Enter the value:=> ");
		scanf("%d",&n);
		a[top] = n;
		top++;
	}
}

// Function to pop out the last entered element

void pop()
{
	if(top == 0)
	{
		printf("Stack is empty");
	}
	else
	{
		top--;
		printf("%d was poped out from stack",a[top]);
	}
}

// Function to display the stack

void display()
{
	int i;
	if(top == 0)
	{
		printf("Stack is empty");
	}
	else
	{
		printf("Displaying the contents of Stack...");
		for(i=top-1;i>=0;i--)
		{
			printf("%d",a[i]);
		}
	}
}
```
