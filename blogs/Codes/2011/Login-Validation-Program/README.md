---
title: Login Validation Program
date: 2011-11-12 07:59:07
categories:
  - Codes
tags:
  - code-example
  - java
  - java-swing
redirectFrom: /2011/11/12/Login-Validation-Program/
---
### Summary

This is a small JAVA Swing GUI login validation program which takes username & password from user and compares with the default values pre-defined. If successful then shows success dialog box else shows failed dialog box.
<!-- more -->
Various swing components are used in making of this program and specially JPasswordField() is the class used to display a password textfield.

### Code

```java
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package swingdemo;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Rohan
 */
public class LoginDemo extends JFrame implements ActionListener {

    String default_username, default_password;
    JLabel l_username, l_password;
    JTextField t_username;
    JPasswordField t_password;
    JButton b_verify, b_reset;
    JPanel p1;

    public LoginDemo() {
        default_username = "student";
        default_password = "stu@dents1";
        Container cp = getContentPane();
        l_username = new JLabel("Username: ");
        l_password = new JLabel("Password: ");
        t_username = new JTextField(15);
        t_password = new JPasswordField(15);
        b_verify = new JButton("Verify");
        b_reset = new JButton("Reset");
        p1 = new JPanel();
        p1.add(l_username);
        p1.add(t_username);
        p1.add(l_password);
        p1.add(t_password);
        p1.add(b_verify);
        p1.add(b_reset);

        b_verify.addActionListener(this);
        b_reset.addActionListener(this);

        cp.add(p1, BorderLayout.CENTER);
    }

    public static void main(String[] args) {
        LoginDemo ld = new LoginDemo();
        ld.setTitle("Login Validation Program");
        ld.setVisible(true);
        ld.setSize(300, 300);
        ld.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == b_verify) {
            String username = "";
            String password = "";
            if (!t_username.getText().equals("")
                    && !t_password.getText().equals("")) {
                username = t_username.getText();
                password = t_password.getText();
                if (username.equals(default_username) && password.equals(default_password)) {
                    JOptionPane.showMessageDialog(null, "Login was successful");
                } else {
                    JOptionPane.showMessageDialog(null, "Login Failed");
                }
            } else {
                if (!t_username.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Username required");
                } else if (!t_password.getPassword().toString().equals("")) {
                    JOptionPane.showMessageDialog(null, "Password required");
                }
            }
        } else if (e.getSource() == b_reset) {
            t_username.setText("");
            t_password.setText("");
        }
    }
}
```
