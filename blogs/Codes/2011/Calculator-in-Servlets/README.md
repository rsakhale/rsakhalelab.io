---
title: Calculator in Servlets
date: 2011-11-10 14:28:36
categories:
  - Codes
tags:
  - code-example
  - java
  - servlets
redirectFrom: /2011/11/10/Calculator-in-Servlets/
---
### Summary

This is a simple Calculator developed using Java Servlets, so it uses the form type in html & uses regex for validating the numerical values.
<!-- more -->
### Code

```java
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package calc;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.*;

/**
*
* @author Administrator
*/
public class Calculator extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        /**
         *
         *[-+]? : Says whether it starts with + or - sign which should be allowed.
         *[0-9]* : Says it can have more than one numerical digit.
         *
         */
         Pattern p = Pattern.compile("^[-+]?[0-9]*");
        try {
            /* TODO output your page here*/
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Calculator</title>");
            out.println("</head>");
            out.println("<body>");


            out.println("<h1>Servlet Calculator</h1><br/>");
            out.println("<form value='Calculator'>");
            out.println("<table border ='0'><tr>");
            out.println("<td>Enter A:</td><td><input type='text' name='a' /></td></tr>");
            out.println("<td>Enter B:</td><td><input type='text' name='b' /></td></tr>");
            out.println("<td>Enter Operation:</td><td><input type='text' name='op' /></td></tr>");
            out.println("<td><input type='submit' /></td></tr>");
            out.println("</table>");
            out.println("</form>");
            String A = request.getParameter("a");
            String B = request.getParameter("b");
            /**
             *
             * Using Matcher it matches with our pattern mentioned earlier
             *
             */
            Matcher ma = p.matcher(A);
            Matcher mb = p.matcher(B);
            String op = request.getParameter("op");
            if (ma.matches() && mb.matches()) {
                if (!op.equals("") && A != null && B != null) {
                    double a = Double.parseDouble(A);
                    double b = Double.parseDouble(B);
                    if (op.equals("add")) {
                        out.println("Answer: <b>" + (a + b) + "</b>");
                    } else if (op.equals("sub")) {
                        out.println("Answer: <b>" + (a - b) + "</b>");
                    } else if (op.equals("mul")) {
                        out.println("Answer: <b>" + (a * b) + "</b>");
                    } else if (op.equals("div")) {
                        out.println("Answer: <b>" + (a / b) + "</b>");
                    } else {
                        out.println("Answer: <font color='RED'>Invalid Operation</font>");
                    }
                } else {
                    out.println("Answer: <font color='CYAN'><b>Some Values are empty</b></font>");
                }
            } else {
                out.println("Answer: <font color='DARKRED'>Invalid Values</font>");
            }
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);


    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);


    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";

    }// </editor-fold>
}
```
