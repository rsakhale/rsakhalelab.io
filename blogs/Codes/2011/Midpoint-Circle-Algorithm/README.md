---
title: Midpoint Circle Algorithm
date: 2011-11-14 18:58:45
categories:
  - Codes
tags:
  - code-example
  - cpp
redirectFrom: /2011/11/14/Midpoint-Circle-Algorithm/
---
### Summary

This is an algorithm for Computer Graphics using C Language to draw a circle using the Midpoint Circle Algorithm
<!-- more -->
### Code

```cpp
#include "stdio.h"
#include "conio.h"
#include "graphics.h"

void plotpts(int x1,int y1,int a1,int b1,int c1)
{
	putpixel(x1+a1,y1-b1,c1);
	putpixel(x1-a1,y1-b1,c1);
	putpixel(x1+a1,y1+b1,c1);
	putpixel(x1-a1,y1+b1,c1);
	putpixel(x1+b1,y1-a1,c1);
	putpixel(x1-b1,y1-a1,c1);
	putpixel(x1+b1,y1+a1,c1);
	putpixel(x1-b1,y1+a1,c1);
}

void ci(int a,int b,int c,int co)
{
	double p = 1-c;
	int xc = 0,yc = c;	
	putpixel(a+xc,b-yc,co);
	putpixel(a-xc,b-yc,co);
	putpixel(a+xc,b+yc,co);
	putpixel(a-xc,b+yc,co);
	while(yc &gt; xc)
	{
		xc++;
		if(p&lt;0)
		{
			p = p + 2 * (xc + 1) + 1;
		}
		else
		{
				p = p + 2 * (xc + 1) + 1 - 2 * (yc - 1);
				yc--;
		}
		plotpts(a,b,xc,yc,co);
	}
}

void main()
{
	int maxx,maxy,gd,gm;
	detectgraph(&gd,&gm);
	gd = DETECT;
	initgraph(&gd,&gm,"C:\TC\BGI");
	maxx = getmaxx()/2;
	maxy = getmaxy()/2;
	ci(maxx,maxy,100,1);
	getch();
	closegraph();
}
```
