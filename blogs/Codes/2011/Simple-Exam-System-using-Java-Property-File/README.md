---
title: Simple Exam System using Java Property File
date: 2011-11-10 14:28:17
categories:
  - Codes
tags:
  - code-example
  - java
  - servlets
redirectFrom: /2011/11/10/Simple-Exam-System-using-Java-Property-File/
---
### Summary

This is a Java Servlet Program example


This program requires a student to authenticate inorder to give the exam, the user details are stored in a JAVA property file as a key value pair mechanism.

<!-- more -->

Another part consists of Questions which the authenticated user has to answer & at the end the user gets the results of the number of questions he entered were right.

### Code

#### Authentication.java

```java
package p1;
 
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
/**
 *
 * @author rpise
 */
@WebServlet(name = "Authentication", urlPatterns = {"/Authentication"})
public class Authentication extends HttpServlet {
 
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here*/
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Exam System : Authentication</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Exam System</h1><br/>"
                    + "<h3>Authentication</h3><br/>"
                    + "<form>");
            out.println("<table border='0'><tbody>"
                    + "<tr><td>Username:</td><td><input type='text' name='username' /></td></tr>"
                    + "<tr><td>Password:</td><td><input type='password' name='password' /></td></tr>"
                    + "<tr><td><input type='submit' value='Submit' /></td></tr>"
                    + "</tbody></table>");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            FileReader fr = new FileReader("D:\batches\JWD\JWD09\ExamSystem\user_records.properties");
            Properties p = new Properties();
            p.load(fr);
            if (username != null & password != null) {
                if (password.equals(p.getProperty(username))) {
                    out.println("<font color='green'>Validation Success</font><br />");
                    ServletContext sc = getServletContext();
                    sc.setAttribute("question", "1");
                    sc.setAttribute("resultset","1");
                    sc.setAttribute("marks", "0");
                    response.sendRedirect("Questions");
                } else {
                    out.println("<font color='red'>Validation Failed</font><br />");
                }
            } else {
                out.println("<font color='darkred'>Error Less Values Passed</font><br />");
            }
 
            out.println("</body>");
            out.println("</html>");
 
        } finally {
            out.close();
        }
    }
 
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
```

#### Questions.java

```java
/********************************
//
// Questions Java Code
//
********************************/


package p1;
 
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
/**
 *
 * @author rpise
 */
@WebServlet(name = "Questions", urlPatterns = {"/Questions"})
public class Questions extends HttpServlet {
 
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Exam System : Questions</title>");
            out.println("</head>");
            out.println("<body>");
            ServletContext sc = getServletContext();
            out.println("<h1>Exam System</h1><br/>"
                    + "<h3>Questions</h3><br/>");
            FileReader fr = new FileReader("D:\batches\JWD\JWD09\ExamSystem\questions_file.properties");
            Properties p = new Properties();
            p.load(fr);
            String quest_value = (String) sc.getAttribute("question");
            String quest = "question" + quest_value;
            if (!quest.equals("question")) {
                if (p.getProperty(quest) != null || quest.equals("question6")) {
 
                    String question = p.getProperty("q" + quest_value);
                    String opt1 = p.getProperty("opt1" + quest_value);
                    String opt2 = p.getProperty("opt2" + quest_value);
                    String opt3 = p.getProperty("opt3" + quest_value);
                    String opt4 = p.getProperty("opt4" + quest_value);
                    if (!quest.equals("question6")) {
                        out.println("Q" + quest_value + ": " + question + "<br/>");
                        out.println("<form action='Questions'>"
                                + "<input type='checkbox' name='opt1' value=2 />" + opt1
                                + "<br/>"
                                + "<input type='checkbox' name='opt2' value=4 />" + opt2
                                + "<br/>"
                                + "<input type='checkbox' name='opt3' value=8 />" + opt3
                                + "<br/>"
                                + "<input type='checkbox' name='opt4' value=16 />" + opt4
                                + "<br />"
                                + "<input type='submit' value='Submit'/>");
                        out.println("</form>");
                    }
                    else
                    {
                        out.println("<Br/><a href='Questions' target='_self'>Check result</a><br/>");
                    }
                    int value = 0;
                    int counter = 0;
                    if (request.getParameter("opt1") != null) {
                        value = value + Integer.parseInt(request.getParameter("opt1"));
                        counter++;
                    }
                    if (request.getParameter("opt2") != null) {
                        value = value + Integer.parseInt(request.getParameter("opt2"));
                        counter++;
                    }
                    if (request.getParameter("opt3") != null) {
                        value = value + Integer.parseInt(request.getParameter("opt3"));
                        counter++;
                    }
                    if (request.getParameter("opt4") != null) {
                        value = value + Integer.parseInt(request.getParameter("opt4"));
                        counter++;
                    }
                    out.println("Counter comes here: " + counter + "<br />");
                    String resultset = (String) sc.getAttribute("resultset");
                    if (value == Integer.parseInt(p.getProperty("ans" + resultset))) {
                        int marks = Integer.parseInt((String) sc.getAttribute("marks"));
                        marks++;
                        sc.setAttribute("marks", Integer.toString(marks));
                        out.println("Yes You scored +1<br/>");
                    }
                    int getValue = Integer.parseInt((String) sc.getAttribute("question"));
                    getValue++;
                    sc.setAttribute("question", Integer.toString(getValue));
                    getValue--;
                    sc.setAttribute("resultset", Integer.toString(getValue));
 
                } else if (Integer.parseInt((String) sc.getAttribute("question")) > 5) {
                    out.println("<br />Result: " + (String) sc.getAttribute("marks"));
                } else {
                    out.println("<br/>Question Not Found<br/>");
                }
 
            }
 
            out.println("</body>");
            out.println("</html>");
 
        } catch (Exception e) {
            out.println("<br/><font color='red'>Error Caught</font><br/>");
        } finally {
            out.close();
        }
    }
 
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
```
