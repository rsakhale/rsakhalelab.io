---
title: Link Shortening Script
date: 2011-06-27 15:18:00
categories:
  - Codes
tags:
  - code-example
  - php
permalink: /codes/2011/link-shortening-script/
redirectFrom: /2011/06/27/Link-Shortening-Script/
---
### Summary

This is a small project quickly made by me after seeing link shortners on web.
<!-- more -->
We require only three files for our small project

1. index.php
1. .htaccess
1. go.php

Also we require a database connection inorder to maintain all the links with their access id's

### Code

#### index.php

```php
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>URL Shortening Project</title>
    </head>
    <body>
        <form action="" method="get">
            <input type="text" name="url" /> &nbsp; <input type="submit" />
        </form>
        <?php

            if($_GET['url'])
            {
                $con = mysql_connect("localhost","root","");
                if(!$con)
                {
                    die('Could not connect: '. mysql_errno());
                }
                mysql_select_db("url",$con);
                $id = uniqid();
                $url = $_GET['url'];
                $sql = "INSERT INTO `url`.`t_link` (`id`, `url`) VALUES ('$id', '$url');";
                mysql_query($sql);
                echo "Your Short URL: http://localhost/url/v-". $id;
                mysql_close($con);
            }
        ?>
    </body>
</html>
```

#### .htaccess

```bash
RewriteEngine On
Options +FollowSymlinks


RewriteRule ^v-(.*)$ go.php?id=$1 [QSA,L,NC]
```

#### go.php

```php
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if ($_GET['id']) {
    $id = $_GET['id'];
    $con = mysql_connect("localhost", "root", "");
    if (!$con) {
        die('Could not connect: ' . mysql_errno());
    }
    mysql_select_db("url", $con);
    $sql = "SELECT * FROM `t_link` where id = '$id'";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    $new_url = $row['url'];
    echo "<html><head><meta HTTP-EQUIV='REFRESH' content='0;url=$new_url'></head></html>";
    mysql_close($con);
}
?>
```

#### SQL Script

```sql
create table url (
	id integer primary key autoincrement,
	access_id varchar (20) not null,
	url varchar (100) not null
);
```

Inorder to run this program XAMPP was used as Local Server.

### Enhancements

This project can be improved further with the following points

1. Adding User Login Details
1. Restricting Guest with limited URL shortening
1. Restricting Guest from making many URL shorten at once by adding flood limit
1. Logged in user has more access and features on making URL short
1. Logged in users can maintain their shortened URL's using control panel
1. Custom URL's can be made for Premium User [Paid/VIP]
1. Adding session for checking logged in users
1. Restricting bots from flooding server load
1. Admin Panel to maintain users & URL's
1. Filter to ban restricted sites
1. Ads on links of free user & premium users link without ads [Full Page Ads]
1. Getting a good custom made skin for this project
1. Listed too many features to make this project much better, if i get time will start implementing one after the other
