---
title: Draw Line using Bresenham's Algorithm
date: 2011-11-12 12:25:40
categories:
  - Codes
tags:
  - code-example
  - cpp
redirectFrom: /2011/11/12/Draw-Line-using-Bresenham-s-Algorithm/
---
### Summary

This is another algorithm which can be used to draw a line and this algorithm is better and gives a more accurate output than DDA algorithm we previously discussed
<!-- more -->
This is another example shown in Computer Graphics using C Language

### Code

```cpp
#include "stdio.h"
#include "conio.h"
#include "graphics.h"


void value(int x1,int y1,int x2,int y2)
{
	int dx,dy,x,y,p,k;
	dx = x2 - x1;
	dy = y2 - y1;
	x = x1;
	y = y1;
	p = (2 * dy) - (dx);
	for(k=0;k&lt;dx;k++)
	{
		if(p&lt;0)
		{
			x++;
			putpixel(x,y,WHITE);
			p = p + (2 * dy);
		}
		else
		{
			x++;
			y++;
			putpixel(x+1,y+1,WHITE);
			p = p + (2 * dy) - (2 * dx);
		}
	}
}

void main()
{
	int gd,gm,xa,xb,ya,yb;
	detechgraph(&gd,&gm);
	gd = DETECT;
	initgraph(&gd,&gm,"C:\\TC\\BGI");
	printf("Enter the starting point:=&gt; ");
	scanf("%d %d",&xa,&ya);
	printf("Enter the end point:=&gt; ");
	scanf("%d %d",&xb,&yb);
	value(xa,ya,xb,yb);
	getch();
	closegraph();
}
```
