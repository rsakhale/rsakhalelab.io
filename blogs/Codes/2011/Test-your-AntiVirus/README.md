---
title: Test your AntiVirus
date: 2011-06-27
categories:
  - Codes
tags:
  - code-example
  - script
redirectFrom: /2011/06/27/Test-your-AntiVirus/
---
### Summary

How to make sure your AntiVirus is working [Windows Based] - A Basic Thing

<!-- more -->
### Code

```
X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*
```
### Save

Save this as `anyname.txt`

If your **AntiVirus** is smart enough, you will get a warning at the same time or scan/run that.

It is probably a fake virus but I`m not sure, anybody know what is this?
