---
title: Email Validation Script
date: 2011-06-27
categories:
  - Codes
tags:
  - code-example
  - java
redirectFrom: /2011/06/27/Email-Validation-Script/
---
### Summary

Email Validator Program developed using pure Object Oriented Programming & String handling in Java during college days as an assignment.
<!-- more -->
### Ability of Validating

#### Username

> _rohan@gmail.com **INVALID**
> .rohan@gmail.com **INVALID**
> rohan$sakhale@gmail.com **INVALID**
> rohan^sakhale@gmail.com **INVALID**
> rohan.@gmail.com **INVALID**
> 12rohan@gmail.com **INVALID**

> rohansakhale@gmail.com **VALID**
> rohan_sakhale@gmail.com **VALID**
> rohan.sakhale@gmail.com **VALID**
> rohan123sakhale@gmail.com **VALID**

#### Special character "@"

> rohan@sakhale@gmail.com **INVALID**
> rohan@@gmail.com **INVALID**
> @@gmail.com **INVALID**
> rohan@gmail.com **VALID**

#### Domain Name

> rohan@.gmail.com **INVALID**
> rohan@a.com **INVALID**
> rohan@aaa.123 **INVALID**
> rohan@aa.co.com.in **INVALID**
> rohan@-gmail.com **INVALID**
> rohan@gmail_gmail.com **INVALID**

> rohan@gmail.com **VALID**
> rohan@gmail.co.in **VALID**
> rohan@123.com**VALID**
> rohan@gmail.in **VALID**
> rohan@rohan-rohan.com **VALID**


### Code

```java
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package emailverifieroop;

import java.io.*;

/**
 *
 * @author RohanSakhale
 */
public class EmailVerifier {

    String s;

    public EmailVerifier() {
        s = "";
    }

    public EmailVerifier(String s) {
        this.s = s;
    }

    void setEmail(String email) {
    	this.s = email;
    }

    /**
     *	Accept string from user
     */
    void accept() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        s = br.readLine();
        br.close();
    }

    /**
     *	Display string entered
     */
    void display() {
        System.out.println("You have entered: " + s);
    }

    /**
     *	Validate username from email
     */
    boolean checkUsername() {
        if (s.length() >= 3) {
            for (int i = 0; i < s.indexOf("@"); i++) {
                if (i != 0 && (s.charAt(i) == '_' || s.charAt(i) == '.')) {
                    continue;
                }
                if ((int) s.charAt(i) < 47 && (int) s.charAt(i) > 58) {
                    if (s.startsWith(Integer.toString(i)) == true) {
                        return false;
                    }
                }
                if ((int) s.charAt(i) < 47) {
                    return false;
                } else if ((int) s.charAt(i) > 57 && (int) s.charAt(i) < 65) {
                    return false;
                } else if ((int) s.charAt(i) > 91 && (int) s.charAt(i) < 97) {
                    return false;
                } else if ((int) s.charAt(i) > 123) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     *	Validate email username
     */
    boolean checkEmailUsername() {
        if (s.indexOf("@") >= 3 && s.charAt(s.indexOf("@") - 1) != '.' && s.charAt(s.indexOf("@") - 1) != '_') {
            for (int i = 0; i < s.indexOf("@"); i++) {
                if (i != 0 && (s.charAt(i) == '_' || s.charAt(i) == '.')) {
                    continue;
                }
                if ((int) s.charAt(i) < 47 && (int) s.charAt(i) > 58) {
                    if (s.startsWith(Integer.toString(i)) == true) {
                        return false;
                    }
                }
                if ((int) s.charAt(i) < 47) {
                    return false;
                } else if ((int) s.charAt(i) > 57 && (int) s.charAt(i) < 65) {
                    return false;
                } else if ((int) s.charAt(i) > 91 && (int) s.charAt(i) < 97) {
                    return false;
                } else if ((int) s.charAt(i) > 123) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     *	Validate "@" exists
     */
    boolean checkPresenceOfSymbol() {
        if (s.indexOf("@") == -1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     *	Validate "@" is not the last character of string
     */
    boolean checkEmailSymbol() {
        if (s.indexOf("@") == s.lastIndexOf("@")) {
            return true;
        } else {
            return false;
        }

    }

    /**
     *	Validate the domain name
     */
    boolean checkDomainName() {
        int count = 0;
        if (s.charAt(s.indexOf("@") + 1) == '_' || s.charAt(s.indexOf("@") + 1) == '-') {
            return false;
        }
        if (s.indexOf(".", s.indexOf("@")) - s.indexOf("@") >= 3) {
            for (int i = s.indexOf("@") + 1; i < s.length(); i++) {
                if (s.charAt(i) == '.') {
                    count++;
                    if (count > 2) {
                        return false;
                    }
                }
            }
            for (int i = s.indexOf("@") + 1; i < s.length(); i++) {
                if ((s.charAt(i) == '.' || s.charAt(i) == '-')
                        || s.indexOf("-") - s.indexOf("@") > 1) {
                    continue;
                }

                if ((int) s.charAt(i) < 47) {
                    return false;
                }

                if ((int) s.charAt(i) > 47 && (int) s.charAt(i) < 57) {
                    continue;

                }
                if ((int) s.charAt(i) < 97) {
                    return false;
                }
                if ((int) s.charAt(i) > 123) {
                    return false;
                }

            }
            if (s.lastIndexOf(".") == s.length() - 4) {
                return true;
            } else if (s.lastIndexOf(".") == s.length() - 3) {
                return true;
            } else if (s.indexOf(".", s.indexOf("@")) == s.length() - 2 && s.lastIndexOf(".") == s.length() - 5) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     *	Validate string as email
     */
    boolean checkEmail() {
        if (!checkEmailUsername()) {
            return false;
        }
        if (!checkPresenceOfSymbol()) {
            return false;
        }
        if (!checkDomainName()) {
            return false;
        }
        if (!checkEmailSymbol()) {
            return false;
        }
        return true;
    }

}
```
