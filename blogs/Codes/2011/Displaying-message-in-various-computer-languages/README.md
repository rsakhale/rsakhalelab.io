---
title: Displaying message in various computer languages
date: 2011-03-24
categories:
  - Codes
tags:
  - code-example
  - php
  - java
  - cpp
redirectFrom: /2011/03/24/Displaying-message-in-various-computer-languages/
---
### Summary

So by starting as a simple post toward my contribution for programming, lets see how messages can be displayed in different programming languages.  
### Hello World in "C"

Using the `printf()` function from `stdio.h` library we write the following instructions for computer to print a message "Hello World

<!-- more -->
```c
#include<stdio.h>  //including Header File
/**
 * Main is the entry point for our program
 */
void main()
{
	//whatever is displayed in double quotes is printed on the console
	printf("Hello World");    
}
```

### Hello World in "C++" 

Using the `cout` object from `iostream.h` library we write the following instructions for computer to print a message "Hello World".

Here the `cout` object uses an operator called insertion ('<<') which is used to print everything within it on the console.

```cpp
#include<iostream.h>  //including Header File 
/**
 * Main is the entry point for our program
 */ 
void main()
{
	//whatever is displayed in double quotes is printed on the console.  
	cout << "Hello World";   
} 
```

### Hello World in Java

Class **System** comes with various console objects like `out`, `in`, `err` which can be used for various purpose. The System library comes from `java.lang` package which is default imported by Java everytime it runs.

The `out` object from **System** class helps to redirect output towards the `console`.

```java
import java.lang.*;  // importing java lang library
class Main
{ 
	public static void main(String [] args) 
	{ 
		System.out.print("Hello World");  
		//To end the message with a new line we use println() method
		System.out.println("This statement will add a new line at the end");
		System.out.print("This will be printed on new line");
	}
}
```

### Hello World in C#

The class System has various objects predefined in C# library, so we use System library & gain the class called "Console" which is attached to the console i.e. Screen & invoke static method called write() using that Console class-name. 

Basic functionality is to print everything within quoutes.

```csharp
using System;
namespace FirstProgram
{
	class Program
	{
		static void main(String [] args)
		{
			Console.Write("Hello World");
			//to end the message with new line we use WriteLine() method
			Console.WriteLine("This should add a new line at the end");
			Console.Write("This message ends with new line");
		}
	}
}
```

### Hello World in PHP 

**PHP** is a powerful language for web, in-order to print any message we just use `echo` keyword & whatever typed in double or single quotes is  displayed as message. Both single & double quotes has separate meaning & we will come across it some other day.

A php program is always enclosed with "`<?php STATMENTS HERE ?>`"

```php
<?php
 echo 'Hello World';
?>
```

So we end up here now, I hope you have come to know how to display message in 5 different languages, I will come back with more languages very soon.
