---
title: Prime numbers between range Java
date: 2011-11-10
categories:
  - Codes
tags:
  - code-example
  - java
  - java-swing
redirectFrom: /2011/11/10/Prime-numbers-between-range-Java/
---
### Summary

Java code using Swing GUI to find prime number between a range using two types of methods

<!-- more -->
### Code

```java
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package primeswing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author Rohan
 */
public class PrimeSwing extends JFrame implements ActionListener {

    int range_number;
    JPanel number, buttons;
    JTextField txtNumber;
    JButton method1, method2;
    JLabel lblNumber;
    JFrame output;
    JLabel lblOutput;

    public PrimeSwing() {
        range_number = 0;
        output = new JFrame("Output of Prime");
        txtNumber = new JTextField(10);
        lblOutput = new JLabel();
        number = new JPanel();
        method1 = new JButton("Method 1");
        method2 = new JButton("Method 2");
        lblNumber = new JLabel("Enter number: ");
        buttons = new JPanel();
        method1.addActionListener(this);
        method2.addActionListener(this);
        number.add(lblNumber);
        number.add(txtNumber);
        add(number);
        buttons.add(method1);
        buttons.add(method2);
        add(buttons, BorderLayout.SOUTH);
        setSize(300, 100);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Find Prime Number");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        new PrimeSwing();
    }

    // Method 2 using static method
    static boolean isPrime(int number) {
        boolean isPrime = false;
        int i = (int) Math.ceil(Math.sqrt(number));
        while (i > 1) {
            if ((number != i) && (number % i == 0)) {
                isPrime = false;
                break;
            } else if (!isPrime) {
                isPrime = true;
            }
            --i;
        }
        return isPrime;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (!txtNumber.getText().equals("")) {
            range_number = Integer.parseInt(txtNumber.getText());
            output.setSize(350, 600);
            output.setVisible(true);
            String temp = "<html>";
            boolean prime;
            if (e.getSource() == method1) {
                temp += "Using Method 1<br />";
                temp += "<ul>";
                for (int i = 2; i < range_number; i++) {
                    prime = true;
                    for (int j = 2; j < i; j++) {
                        if (i % j == 0 && i != j) {
                            prime = false;
                            break;
                        }
                    }
                    if (prime) {
                        temp = temp + "<li>" + i + "</li>";
                    }
                }
                temp += "</ul>";
            }
            else if(e.getSource() ==  method2)
            {
                temp += "Using Method 2<br />";
                temp += "<ul>";
                for (int i = 1; i < range_number; i++) {
                    if(PrimeSwing.isPrime(i))
                    {
                        temp = temp + "<li>" + i + "</li>";
                    }
                }
                temp += "</ul>";
            }
            temp = temp + "</html>";
            lblOutput.setText(temp);
            output.add(lblOutput);
        } else {
            JOptionPane.showMessageDialog(this, "Please enter some number");
        }
    }
}
```
