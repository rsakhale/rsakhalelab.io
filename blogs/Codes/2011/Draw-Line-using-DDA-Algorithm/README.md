---
title: Draw Line using DDA Algorithm
date: 2011-11-12 12:11:39
categories:
  - Codes
tags:
  - code-example
  - cpp
redirectFrom: /2011/11/12/Draw-Line-using-DDA-Algorithm/
---
### Summary

This is a computer graphics related algorithm that draws line using graphics in C language.

Using this algorithm we won't get the exact required line, there are in-built graphics functions present in graphics.h header that gives a more better line.
<!-- more -->
But in-order to master algorithms we need to get a start with whatever we can learn with.


### Code

```cpp
#include "stdio.h"
#include "conio.h"
#include "graphics.h"
#include "math.h"
#define ROUND(a) ((int)(a+0.5))

void dda_line(int xa,int xb,int ya,int yb)
{
	int dx,dy,i,length;
	float ddx,ddy,x,y;
	
	dx = xb - xa;
	dy = yb - ya;
	
	x = xa;
	y = ya;
	if(abs(dx) &gt; abs(dy))
	{
		length = abs(dx);
	}
	else
	{
		length = abs(dy);
	}
	
	ddx = abs(dx)/length;
	ddy = abs(dy)/length;
	
	putpixel(ROUND(x),ROUND(y),RED);
	i=0;
	while(i&lt;=length)
	{
		x = x + ddx;
		y = y + ddy;
		putpixel(ROUND(x),ROUND(y),RED);
		i++;
	}
	putpixel(ROUND(x),ROUND(y),RED);
}

void main()
{	
	int xa,xb,ya,yb;
	int gd,gm;
	detectgraph(&gd,&gm);
	gd = DETECT;
	initgraph(&gd,&gm,"C:\TC\BGI");
	
	printf("Enter start & end points:=&gt; ");
	scanf("%d %d",&xa,&ya);
	scanf("%d %d",&xb,&yb);
	
	dda_line(xa,xb,ya,yb);
	getch();
	closegraph();
}
```
