---
title: Moving Stickman
date: 2011-06-27 16:17:00
categories:
  - Codes
tags:
  - cpp
  - code-example
redirectFrom: /2011/06/27/Moving-Stickman/
---
### Summary

Moving Stickman code in C Language using **Visual Studio**.
<!-- more -->
Customized functions for Visual Studio 2010 to act same as Turbo C

> `textcolor()` "Changes text color"
> `clrscr()` "Clears screen output"
> `gotoxy()` "Move cursor to x,y position"

This stickman moves on keys.

> `w` ==> up
> `a` ==> left
> `s` ==> down
> `d` ==> right
> `c` ==> changes the color

> Press any key and change character of stickman

### Code

```cpp
// MovingStickMan.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <Windows.h>

// Clear screen output
void clrscr()
{
	system("cls");
}

// move cursor to x,y co-ordinates
void gotoxy(int a,int b)
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD Cord;
	Cord.X = a;
	Cord.Y = b;
	SetConsoleCursorPosition(hStdOut,Cord);
}

// Change text color
void textcolor(int a)
{
	int fontcolor = a;
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	int color_A = 0;
	color_A = _rotl(color_A,4) | fontcolor;
	SetConsoleTextAttribute(hStdOut,a);
}

// Define global variables
char c;  
int x=10,y=5;  
int color=1;  
      
/**
 *	Draw Sleeping Line with
 *
 *	@param x "X co-ordinate of cursor position"
 *	@param y "Y co-ordinate of cursor position"
 *	@param len "Length of sleeping line to print"
 *	@param ch "Character to be used for printing"
 */
void sl(int x,int y,int len,char ch)  
{
	int i=0;
	for(i=0;i<len;i++)
	{
		gotoxy(x+i,y);
		putch(ch);
	}  
}

/**
 *	Draw Sleeping Line with
 *
 *	@param x "X co-ordinate of cursor position"
 *	@param y "Y co-ordinate of cursor position"
 *	@param len "Length of vertical line to print"
 *	@param ch "Character to be used for printing"
 */
void vl(int x,int y,int len,char ch)
{
	int i=0;
	for(i=0;i<len;i++)
	{
		gotoxy(x,y+i);
		putch(ch);
	}
}  

/**
 *	Draw stickman
 *
 *	@param x "Starting x co-ordinate of stickman"
 *	@param y "Starting y co-ordinate of stickman"
 *	@param a "Character used for priting stickman"
 */
void sm(int x,int y,char a)
{
	sl(x,y,5,a);
	vl(x,y+1,3,a);
	sl(x+1,y+3,3,a);
	vl(x+4,y+1,3,a);
	vl(x+2,y+4,7,a);
	sl(x-3,y+6,11,a);
	vl(x-3,y+7,1,a);
	vl(x+7,y+7,1,a);
	sl(x-1,y+11,7,a); 
	vl(x-1,y+12,3,a);
	vl(x+5,y+12,3,a);
}

/**
 *	Move stickman to new position
 *
 *	@param m "Operation used to move stickman"
 */
void move(char m)
{
	switch(m)
	{
	case 'w':
		sm(x,y,' ');
		y--;
		sm(x,y,c);
		break;
	case 'a':
		sm(x,y,' ');
		x--;
		sm(x,y,c);
		break;
	case 's':
		sm(x,y,' ');
		y++;
		sm(x,y,c);
		break;
	case 'd':
		sm(x,y,' ');
		x++;
		sm(x,y,c);
		break;
	case 'c':
		if(color==16)
			color=1;
		else
			color++;
		textcolor(color);
		sm(x,y,c);
		break;
	default:
		c = m;
		sm(x,y,c);
	}
}

/**
 *	Entry point of application
 */
int _tmain(int argc, _TCHAR* argv[])
{
	char mv;
	clrscr();
	gotoxy(15,15);
	printf("Enter character for Stickman==> ");
	scanf("%c",&c);
	clrscr();
	sm(x, y, c);
	// Iterate the loop till use presses `q` for exit
	do
	{
		mv = getch();
		move(mv);
	}while(mv!='q');
	getch();
	return 0;
}  
```
