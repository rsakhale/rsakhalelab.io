---
title: Pattern Printing 3
date: 2012-03-21 20:51:40
categories:
  - Codes
tags:
  - code-example
  - csharp
  - pattern-printing
redirectFrom: /2012/03/21/Pattern-Printing-3/
---
### Summary

The following pattern is in C#, as it was asked by one of my friend.

Prints the pattern in the following way

<!-- more -->
```
12345
2345
345
45
5
```

### Screenshot

![Pattern Printing Output](/images/pattern_3_csharp_output.jpg)

### Code

```csharp
using System;

namespace TestApplication
{
    class Pattern
    {
        public static void Main()
        {
            for (int i = 1; i <= 5; i++)
            {
                for (int j = i; j <= 5; j++)
                {
                    Console.Write(j);
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
```
