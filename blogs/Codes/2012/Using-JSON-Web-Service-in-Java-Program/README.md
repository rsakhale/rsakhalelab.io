---
title: Using JSON Web Service in Java Program
date: 2012-05-24 18:22:05
categories:
  - Codes
tags:
  - code-example
  - java
redirectFrom: /2012/05/24/Using-JSON-Web-Service-in-Java-Program/
---
### Summary

Before knowing on how to use the JSON Web Service, you can read this article on [how to create JSON Web Service]().

So for using JSON Web Service, you need to have an external library imported into your project which can be used easily from [JSON Documentation](http://www.json.org/java/index.html).

<!-- more -->
Also to make HTTP request easily, we can make use Apache Java library for the same which can be downloaded from [Apache Downloads](https://hc.apache.org/downloads.cgi)

Once your import's are done, you can use the following code to use the json data taken from a webservice.

### Screenshot

![Using JSON Web Service in Java](/images/output_http_json_java.jpg)

### HTTPJsonTest.java

```java
package httpjsontest;

import org.json.JSONArray;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

/**
 *
 * @author RohanSakhale
 */
public class HTTPJsonTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // Create HttpClient imported from apache library
        HttpClient client = new DefaultHttpClient();
        // Create get object for that by giving your web service URL
        HttpGet get = new HttpGet("http://demo.rohansakhale.com/getteacherdata.php");
        /* 
         * The above url is without any teacher id parameter which means retrieve all teachers data
         */

        /* Now we will execute the HttpGet request & store the response as HttpResponse Object
         */
        HttpResponse response = client.execute(get);

        /* Now we will take all the data from response as a string
         */
        String data = EntityUtils.toString(response.getEntity());

        // Response has data stored in the form of Entity which we retrieve by above method

        // Make sure you use org.json library strictly to use below constructor
        /* The string we got from http respnse
         * we passed it to create JSONArray
         */
        JSONArray jsonA = new JSONArray(data);

        JSONObject temp;
        int id;
        String name, address, qualification, contact;
        for (int i = 0; i < jsonA.length(); i++) {
            // Get array element as JSONObject
            temp = jsonA.getJSONObject(i);
            
            /* get data by using the column 
             * name of database as the key
             * and it will return the value pointing it
             */
            id = temp.getInt("id");
            name = temp.getString("name");
            address = temp.getString("address");
            qualification = temp.getString("qualification");
            contact = temp.getString("contact");
            System.out.println("ID: " + id);
            System.out.println("Name: " + name);
            System.out.println("Address: " + address);
            System.out.println("Qualification: " + qualification);
            System.out.println("Contact: " + contact);
        }
    }
}
```
