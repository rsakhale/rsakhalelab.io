---
title: Stack Program for Android
date: 2012-03-17 16:31:14
categories:
  - Codes
tags:
  - code-example
  - java
  - android_stack_output
redirectFrom: /2012/03/17/Stack-Program-for-Android/
---
### Summary

My first android program showing the stack mechanism.

You can add a new value say Push it in a stack, pop the last entered value.

This stack has a limit of 5.

<!-- more -->
### Screenshot

![Android Stack Output](/images/android_stack_output.jpg)

The Code below only represents the main class that contains the basic logic, the layout is handled using xml files & the android program flow is managed using android manifest file.

### Code

#### StackActivity.java

```java
package com.rohansakhale.stack;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class StackActivity extends Activity {
    /** Called when the activity is first created. */
	
	int stack_position = 0;
	int [] value = new int[5]; // Created a stack of size 5
	TextView display;
	Button push,pop;
	EditText getValue;
	TextView operation;
	
	// Push method is used to add the value to our stack and places it at the top	
	public void push(int number)
	{
		value[stack_position] = number;
		operation.setText("Operation: Successfully Added New Value");
		stack_position++;
	}
	
	// Pop method is used to remove the last value entered in the stack
	public void pop()
	{
		stack_position--;
		operation.setText("Operation: Successfully Poped Out: " + value[stack_position]);
		value[stack_position] = 0;	
	}
	
	// Print_stack method is used to print the entire stack
	public void print_stack()
	{
		String temp = "Stack";
		for(int i = stack_position-1;i >= 0 ; i--)
		{
			temp = temp + "\n" + value[i];
		}
		display.setText(temp);
	}
	
	// Displays error or success message
	public void print_error(String err)
	{		
		operation.setText("Operation: " + err);
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        // Initialize our data members
        display = (TextView) findViewById(R.id.display);
        push = (Button) findViewById(R.id.push);
        pop = (Button) findViewById(R.id.pop);
        getValue = (EditText) findViewById(R.id.editText1);
        operation = (TextView) findViewById(R.id.operationDisplay);
        
        // Handling the User Clicking Events 
        push.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!getValue.getText().toString().equals(""))
				{
					if(stack_position < 5)
					{
						push(Integer.parseInt(getValue.getText().toString()));						
						print_stack();
					}	
					else
					{
						 print_error("Stack is full");
					}	
					getValue.setText("");
				}
				else
				{
					print_error("Entered some value in Text Field");
				}
			}
		});
        
        pop.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {

					if(stack_position > 0)
					{
						pop();					
						print_stack();					
					}	
					else
					{
						print_error("Stack is Empty");
					}					
			}
		});
        
    }
}
```

#### main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="fill_parent"
    android:layout_height="fill_parent"
    android:orientation="vertical" >



    <TextView
        android:id="@+id/display"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content"
        android:text="@string/hello"
        android:textSize="30dp" />

    <EditText
        android:id="@+id/editText1"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:inputType="number" >

        <requestFocus />
    </EditText>

    <Button
        android:id="@+id/push"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="@string/push"
        android:textSize="25dp" />

    <Button
        android:id="@+id/pop"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="@string/pop"
        android:textSize="25dp" />

    <TextView
        android:id="@+id/operationDisplay"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/operation" />

</LinearLayout>
```
