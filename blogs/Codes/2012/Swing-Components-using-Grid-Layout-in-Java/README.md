---
title: Swing Components using Grid Layout in Java
date: 2012-06-27 15:25:17
categories:
  - Codes
tags:
  - code-example
  - java
  - java-swing
redirectFrom: /2012/06/27/Swing-Components-using-Grid-Layout-in-Java/
---
### Summary

Another Swing program demo where I have used 3 layouts to show up the demo which are **BorderLayout, BoxLayout, GridLayout**.

### Border Layout

A layout container that arranges and resizes its components to fit in five regions: **north, south, east, west, and center**.

<!-- more -->
### Box Layout

A layout that arranges components either on top of each other or in a row.

### Grid Layout

This layout places components in a grid of cells.


The current demo app uses the following list of Swing Components, where in each does the job as they are named

- JFrame
- JLabel
- JButton
- JTextField
- JPassword
- JComboBox
- JCheckBox
- JList
- JPanel
- JScrollPane


The button has been given an ActionListener which displays all the components in a Message Dialog made up using JOptionPane. It also shows how each component is being used.

### Screenshot

![Swings & Layout Demo in Java](/images/output_swing_demo_2.jpg)

### Code

```java
package com.rohansakhale.javaquicktest.swingdemo;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author Rohan Sakhale
 */
public class SwingDemo {

    JFrame mainWindow;
    JLabel lPersonName, lPersonPassword, lPersonHobbies, lPersonGender, lPersonQualification;
    JComboBox<String> gender;
    JCheckBox cricket, reading, dancing, swimming, gaming;
    JList<String> qualification;
    JTextField personName;
    JPasswordField personPassword;
    JButton submit;
    JPanel mainPanel, hobbies;
    JScrollPane jHobbies, jQualification;

    public SwingDemo() {

        //Initializing Components
        mainWindow = new JFrame("Swing Demo by rohansakhale");
        submit = new JButton("Submit Data");

        lPersonName = new JLabel("Enter Person Name:");
        personName = new JTextField(10);

        lPersonPassword = new JLabel("Enter Password: ");
        personPassword = new JPasswordField(10);

        lPersonGender = new JLabel("Select Gender");
        String[] sGender = {"Male", "Female"};
        gender = new JComboBox<String>(sGender);

        lPersonHobbies = new JLabel("Choose Hobbies");
        cricket = new JCheckBox("Cricket");
        dancing = new JCheckBox("Dancing");
        reading = new JCheckBox("Reading");
        swimming = new JCheckBox("Swimming");
        gaming = new JCheckBox("Gaming");

        lPersonQualification = new JLabel("Select Qualification:");
        String[] sQualification = {"SSC", "HSC", "Gradudate", "Post-Graduate", "PHD"};
        qualification = new JList<String>(sQualification);
        qualification.setAutoscrolls(true);
        jQualification = new JScrollPane(qualification);
        jQualification.setVerticalScrollBar(new JScrollBar(JScrollBar.VERTICAL));

        hobbies = new JPanel();
        hobbies.setLayout(new BoxLayout(hobbies, BoxLayout.Y_AXIS));

        jHobbies = new JScrollPane(hobbies);
        jHobbies.setVerticalScrollBar(new JScrollBar(JScrollBar.VERTICAL));

        mainPanel = new JPanel();
        GridLayout g = new GridLayout(0, 2, 5, 5);

        mainPanel.setLayout(g);

        mainPanel.add(lPersonName);
        mainPanel.add(personName);
        mainPanel.add(lPersonPassword);
        mainPanel.add(personPassword);
        mainPanel.add(lPersonGender);
        mainPanel.add(gender);

        hobbies.add(cricket);
        hobbies.add(dancing);
        hobbies.add(reading);
        hobbies.add(swimming);
        hobbies.add(gaming);

        mainPanel.add(lPersonHobbies);
        mainPanel.add(jHobbies);
        mainPanel.add(lPersonQualification);
        mainPanel.add(jQualification);

        mainWindow.add(mainPanel);
        mainWindow.add(BorderLayout.EAST, new JPanel());
        mainWindow.add(BorderLayout.WEST, new JPanel());
        mainWindow.add(BorderLayout.NORTH, new JPanel());

        mainWindow.add(BorderLayout.SOUTH, submit);

        // Add ActionListener to submit button
        submit.addActionListener(new SubmitData());

        // Setting window size & visiblibility
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindow.setSize(350, 400);
        mainWindow.setVisible(true);
    }

    private class SubmitData implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String output = ";
            output = "Name: " + personName.getText() + "\n";
            output += "Password: " + String.valueOf(personPassword.getPassword()) + "\n";
            output += "Gender: " + gender.getSelectedItem().toString() + "\n";
            output += "Hobbies: ";
            if (cricket.isSelected()) {
                output += cricket.getText() + " ";
            }
            if (dancing.isSelected()) {
                output += dancing.getText() + " ";
            }
            if (reading.isSelected()) {
                output += reading.getText() + " ";
            }
            if (swimming.isSelected()) {
                output += swimming.getText() + " ";
            }
            if (gaming.isSelected()) {
                output += gaming.getText() + " ";
            }
            output += "\n";
            output += "Qualification: ";
            for (String v : qualification.getSelectedValuesList()) {
                output += v + " ";
            }
            JOptionPane.showMessageDialog(mainWindow, output, "Result Data", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public static void main(String[] args) {
        new SwingDemo();
    }
}
```
