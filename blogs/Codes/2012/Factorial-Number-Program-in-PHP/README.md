---
title: Factorial Number Program in PHP
date: 2012-06-06 00:02:03
categories:
  - Codes
tags:
  - code-example
  - php
redirectFrom: /2012/06/06/Factorial-Number-Program-in-PHP/
---
### Summary

Simple program to get factorial number of any desired number by user.
<!-- more -->
The following code makes use of a function and with the help of loops and other variables returns the calculated Factorial value of the number.

### Screenshot

![Factorial number in PHP Screenshot](/images/factorial_output.jpg)

### Code

```php
<?
/** 
 *  Function to get Factorial of a Number
 *  Rohan Sakhale
 *  17th March 2012
 */

function getFactorial($num)
{
	$fact = 1;
	for($i = 1; $i <= $num ;$i++)
		$fact = $fact * $i;
	return $fact;
}
?>
<!doctype html>
<html>
    <head>
        <title>Factorial Program in PHP</title>
    </head>
<body>
    <form action="" method="post">
        Enter the number whose factorial requires to be found<br />
        <input type="number" name="number" id="number" maxlength="4" autofocus required/>
        <input type="submit" name="submit" value="Submit" />
    </form>
<?
if(isset($_POST['submit']) and $_POST['submit'] == "Submit")
{
	if(isset($_POST['number']) and is_numeric($_POST['number']))
	{
		echo 'Factorial Number: <strong>'.getFactorial($_POST['number']).'</strong>';
	}
	else
	{
		echo 'You need to enter number';
	}
}

?>
</body>
</html>
```
