---
title: Swing Demo Program
date: 2012-05-27 14:38:25
categories:
  - Codes
tags:
  - code-example
  - java
  - java-swing
redirectFrom: /2012/05/27/Swing-Demo-Program/
---
### Summary

The following code displays various usage of Swing Components. 

This is done in my first Advanced Java practical assignment, so just thought would be useful for others.

Focuses on using various Swing Components and the GridBagLayout usage.

<!-- more -->
### Screenshot

![Java Swing Demo](/images/output_swing_demo.jpg)

### Code

```java
package swingdemo;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Rohan Sakhale
 */
public class SwingDemo extends JFrame implements ActionListener {

    JLabel l_name, l_address, l_phone, l_gender, l_hobbies, l_language, l_dob;
    JTextField t_name, t_phone;
    JTextArea t_address;
    JRadioButton r_male, r_female;
    JCheckBox c_cricket, c_football;
    JButton b_submit, b_reset;
    JPanel p_radio, p_check, p_dob;
    JComboBox co_day, co_month, co_year;
    JList li_language;
    ButtonGroup bg;
    JTable exam_table;

    public SwingDemo() {
        /*
         * 
         * Initialization
         * 
         */
        Container cp = getContentPane();
        cp.setLayout(new GridBagLayout());
        l_name = new JLabel("Name: ");
        l_address = new JLabel("Address: ");
        l_phone = new JLabel("Phone No: ");
        l_gender = new JLabel("Gender: ");
        l_hobbies = new JLabel("Hobbies: ");
        l_language = new JLabel("Languages: ");
        l_dob = new JLabel("D.O.B.: ");
        t_name = new JTextField(15);
        t_phone = new JTextField(10);
        t_address = new JTextArea(3, 13);
        r_male = new JRadioButton("Male");
        r_female = new JRadioButton("Female");
        c_cricket = new JCheckBox("Cricket");
        c_football = new JCheckBox("Football");
        b_submit = new JButton("Submit");
        b_reset = new JButton("Reset");
        p_radio = new JPanel();
        p_check = new JPanel();
        p_dob = new JPanel();
        co_day = new JComboBox();
        for (int i = 1; i <= 31; i++) {
            co_day.addItem(i);
        }
        co_month = new JComboBox();
        for (int i = 1; i <= 12; i++) {
            co_month.addItem(i);
        }
        co_year = new JComboBox();
        for (int i = 1950; i <= 2005; i++) {
            co_year.addItem(i);
        }
        String lang[] = {"ERP", "VisualBasic", "AdvancedJava", "SQL", "IS"};
        li_language = new JList(lang);
        String colHead[] = {"Subject", "Marks"};
        String data[][] = {{"Visual Basic", "65"}, {"Java", "72"}, {"CSharp", "85"}, {"Scala", "76"}, {"Adobe", "92"}};
        exam_table = new JTable(data, colHead);

        bg = new ButtonGroup();
        bg.add(r_male);
        bg.add(r_female);
        p_radio.add(r_male);
        p_radio.add(r_female);
        p_check.add(c_cricket);
        p_check.add(c_football);
        p_dob.add(co_day);
        p_dob.add(co_month);
        p_dob.add(co_year);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10, 10, 10, 10);
        gbc.gridx = 0;
        gbc.gridy = 0;
        cp.add(l_name, gbc);
        gbc.gridx = 1;
        gbc.gridy = 0;
        cp.add(t_name, gbc);
        gbc.gridx = 0;
        gbc.gridy = 1;
        cp.add(l_address, gbc);
        gbc.gridx = 1;
        gbc.gridy = 1;
        cp.add(t_address, gbc);
        gbc.gridx = 0;
        gbc.gridy = 2;
        cp.add(l_phone, gbc);
        gbc.gridx = 1;
        gbc.gridy = 2;
        cp.add(t_phone, gbc);
        gbc.gridx = 0;
        gbc.gridy = 3;
        cp.add(l_gender, gbc);
        gbc.gridx = 1;
        gbc.gridy = 3;
        cp.add(p_radio, gbc);
        gbc.gridx = 0;
        gbc.gridy = 4;
        cp.add(l_hobbies, gbc);
        gbc.gridx = 1;
        gbc.gridy = 4;
        cp.add(p_check, gbc);
        gbc.gridx = 0;
        gbc.gridy = 5;
        cp.add(l_dob, gbc);
        gbc.gridx = 1;
        gbc.gridy = 5;
        cp.add(p_dob, gbc);
        gbc.gridx = 0;
        gbc.gridy = 6;
        cp.add(l_language, gbc);
        gbc.gridx = 1;
        gbc.gridy = 6;
        cp.add(li_language, gbc);
        gbc.gridx = 1;
        gbc.gridy = 7;
        cp.add(exam_table, gbc);
        gbc.gridx = 0;
        gbc.gridy = 8;
        cp.add(b_submit, gbc);
        gbc.gridx = 1;
        gbc.gridy = 8;
        cp.add(b_reset, gbc);

        b_submit.addActionListener(this);
        b_reset.addActionListener(this);
    }

    public static void main(String[] args) {
        // TODO code application logic here
        SwingDemo sd = new SwingDemo();
        sd.setTitle("Swing Demo Program by rohansakhale");
        sd.setSize(400, 610);
        sd.setVisible(true);
        sd.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == b_submit) {
            if (t_name.getText().equals("") || t_phone.getText().equals("")
                    || t_address.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Make sure you fill all the textboxes", "Error", JOptionPane.ERROR_MESSAGE);
            } else if (r_male.isSelected() == false && r_female.isSelected() == false) {
                JOptionPane.showMessageDialog(null, "Make sure you select the gender", "Error", JOptionPane.ERROR_MESSAGE);

            } else if (c_football.isSelected() == false && c_cricket.isSelected() == false) {
                JOptionPane.showMessageDialog(null, "Make sure you select the hobbies", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Submission was success");
            }
        } else if (e.getSource() == b_reset) {
            t_name.setText("");
            t_phone.setText("");
            t_address.setText("");
            c_cricket.setSelected(false);
            c_football.setSelected(false);
            r_male.setSelected(false);
            r_female.setSelected(false);
        }
    }
}
```
