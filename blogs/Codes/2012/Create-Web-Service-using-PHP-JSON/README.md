---
title: Create Web Service using PHP & JSON
date: 2012-05-24 16:36:40
categories:
  - Codes
tags:
  - code-example
  - php
redirectFrom: /2012/05/24/Create-Web-Service-using-PHP-JSON/
---
### Summary

The following program will help us create a web service which can be called via HTTP calls from any language example Java application that can be a mobile app, desktop app or web app.
<!-- more -->
**Goal is to retrieve data stored on a server**

We will consider an example of a database having information of teachers.

So our database will have a table name `teacher` which will have fields called `id` -> **teacher id**, `name` -> **teacher name**, `address` -> **address of teacher**, `qualification` -> **qualification of teacher**, `contact` -> **contact number of teacher** etc.

So when we make request to our web service by giving additional information via url we can retrieve details of particular teacher or all teachers.

The output data after requesting the server will be given into json format which can be accessed very easily from any language like Java etc.

#### [DEMO Get Teacher Data](http://demo.rohansakhale.com/getteacherdata.php)

You can also read on [Using JSON Web Service in Java](#)

### Code

```php
/*
First we will see if the user is requesting particular data or he wants all the data
So we will use get method parameters to retrieve single teacher data by taking his id
*/

$query = "select * from `teachers`";
/*
Query to retrieve all teachers data
*/

if(isset($_GET['id'])){
	// if teacher id exists, search by id only to give precised results
	$query .= " where `id` = " . $_GET['id'];
}

//Execute and store resultant object
$result = $con->query($query);

//check if result is more than 0
if($result && $result->num_rows > 0){
	$arr = [];
	// create arr object
	while($row = $result->fetch_assoc())
	{
		$arr[] = $row;
		// add entire row of sql resultant as element into array object
	}
	echo json_encode($arr);
	// output the entire arr into json format
}else{
	echo json_encode(['error' => 'invalid teacher id']);
	// Display invalid id given
}
```
