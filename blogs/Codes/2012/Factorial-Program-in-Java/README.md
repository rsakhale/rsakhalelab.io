---
title: Factorial Program in Java
date: 2012-06-16 15:31:33
categories:
  - Codes
tags:
  - code-example
  - java
redirectFrom: /2012/06/16/Factorial-Program-in-Java/
---
### Summary

In mathematics, the factorial of a non-negative integer n, denoted by n!, is the product of all positive integers less than or equal to n.
<!-- more -->
### Example

```
5! = 5 x 4 x 3 x 2 x 1 = 120
```

The following code creates a GUI for calculating factorial numbers upto 170.

The user has to enter the number and click on Calculate which displays the answer on JLabel below the button.

The Class has been designed by extending JPanel which can be called from other classes and added onto the JFrame for displaying.

<!-- ### Screenshot

![Factorial Program in Java](/images/output_factorial_java.jpg) -->

### Code

```java
package com.rohansakhale.fact;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author RohanSakhale
 */
public class Factorial extends JPanel {

    JTextField tfNumber;
    JLabel lEnterNumber, lAnswer;
    JButton bCalc;
    JPanel number, collection;

    public Factorial() {
        tfNumber = new JTextField(15);
        lEnterNumber = new JLabel("Enter Number: ");
        lAnswer = new JLabel("Answer: ");
        bCalc = new JButton("Calculate");
        number = new JPanel();
        collection = new JPanel();
        collection.setLayout(new BoxLayout(collection, BoxLayout.Y_AXIS));
        number.add(lEnterNumber);
        number.add(tfNumber);
        collection.add(number);
        collection.add(bCalc);
        collection.add(lAnswer);

        bCalc.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (!tfNumber.getText().equals(")) {
                    double num = Integer.parseInt(tfNumber.getText());
                    if(num <= 170){
                        double fact = 1;
                        for(double i = 1;i<=num;i++){
                            fact = fact * i;
                        }
                        lAnswer.setText("Answer: " + fact);
                        tfNumber.setText(");
                        tfNumber.requestFocus();
                    }else{
                        JOptionPane.showMessageDialog(null, "Please enter number upto 170", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Please enter number upto 170", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        add(collection);
    }
}

class FactorialDemo{
    public static void main(String[] args) {
        JFrame factFrame = new JFrame("Factorial Demo Program");
        Factorial fact = new Factorial();
        factFrame.add(fact);
        factFrame.setSize(400,150);
        factFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        factFrame.setVisible(true);
    }
}

```
