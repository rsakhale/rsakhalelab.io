---
title: Calculate Fahrenheit to Celsius Temperature in Java
date: 2012-06-16 15:31:03
categories:
  - Codes
tags:
  - code-example
  - java
  - java-swing
redirectFrom: /2012/06/16/Calculate-Fahrenheit-to-Celsius-Temperature-in-Java/
---
### Summary

In-order to convert from Fahrenheit to Celsius, we have to apply a simple logic onto Celsius "Deduct 32, then multiply by 5, then divide by 9".

Our code provides a simple GUI, where the user has to enter the temperature in Fahrenheit and after clicking on convert displays the Celsius temperature below button in the form of JLabel.

<!-- more -->
The Class has been designed by extending JPanel which can be called from other classes and added onto the JFrame for displaying.

### Screenshot

![Calculate Fahrenheit to Celsius Temperature in Java](/images/output_fahrenheit_celsius_java.jpg)

### Code

```java
package com.rohansakhale.treedemo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author RohanSakhale
 */
public class F2C extends JPanel{
    JTextField tfInputTemp;
    JLabel lInputTemp, lAnswer;
    JButton bCalc;
    JPanel InputTemp, collections;
    
    public F2C() {
        tfInputTemp = new JTextField(15);
        lInputTemp = new JLabel(\"Enter Temperature in Fahrenheit: \");
        lAnswer = new JLabel(\"Answer: \");
        bCalc = new JButton(\"Convert\");
        InputTemp = new JPanel();
        collections = new JPanel();
        collections.setLayout(new BoxLayout(collections, BoxLayout.Y_AXIS));
        InputTemp.add(lInputTemp);
        InputTemp.add(tfInputTemp);
        collections.add(InputTemp);
        collections.add(bCalc);
        collections.add(lAnswer);
        bCalc.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!tfInputTemp.getText().equals(\"\")) {
                    double temp = Double.parseDouble(tfInputTemp.getText());
                    double celsius = (temp - 32 ) * 5 / 9;
                    lAnswer.setText(\"Answer: \" + celsius + \" C\");
                }
            }
        });
        add(collections);
    }
}

class F2CMainDemo{
        public static void main(String[] args) {
        JFrame f2cFrame = new JFrame(\"Celsius to Fahrenheit Demo Program\");
        F2C f2c = new F2C();
        f2cFrame.add(f2c);
        f2cFrame.setSize(400, 150);
        f2cFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f2cFrame.setVisible(true);
    }
}
```
