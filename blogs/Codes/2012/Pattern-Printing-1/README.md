---
title: Pattern Printing 1
date: 2012-03-17 16:00:21
categories:
  - Codes
tags:
  - code-example
  - cpp
  - pattern-printing
redirectFrom: /2012/03/17/Pattern-Printing-1/
---
### Summary

I will be sharing multiple patterns with a sequence starting from #1 to lets see how far it goes.

The pattern that I will be printing in the following code was asked during my exams.
<!-- more -->
It will print numbers in the following way

### Screenshot

![Pattern Printing #1](/images/pattern1.jpg)

### Code

```cpp
#include "iostream.h"
#include "conio.h"

void main()
{
	int i,j,k;
	clrscr();
	for(i=1; i <= 5; i++)
	{
		for(j=0; j < i; j++)
		{
			cout << (i+j) << " ";
		}
		for(k=(i+j)-2; k > (i-1); k--)
		{
			cout << k << " ";
		}
		cout << endl;
	}
	getch();
}
```
