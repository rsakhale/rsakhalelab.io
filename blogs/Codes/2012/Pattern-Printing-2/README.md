---
title: Pattern Printing 2
date: 2012-03-21 20:52:50
categories:
  - Codes
tags:
  - code-example
  - php
  - pattern-printing
redirectFrom: /2012/03/21/Pattern-Printing-2/
---
### Summary

The following pattern is coded in php, as it was asked by one of my friend.

Prints the multiples in the following way.

<!-- more -->
```
0
0 2
0 3 6
0 4 8 12
0 5 10 15 20
0 6 12 18 24 30
0 7 14 21 28 35 42
```

### Screenshot

![Pattern #2 PHP Output](/images/pattern_2_php_output.jpg)

### Code

```php
<?php
for($i = 1; $i <= 7; $i++)
{
	for($j = 0; $j < $i; $j++)
	{
		echo $i * $j. ' ';
	}
	// Print New Line using br tag
	echo '<br>';
}
?>
```
