---
title: Program to find Perfect Number's upto 1000
date: 2012-03-12 22:50:10
categories:
  - Codes
tags:
  - code-example
  - csharp
redirectFrom: /2012/03/12/Program-to-find-Perfect-Number-s-upto-1000/
---
### Summary

**Perfect Number** is a positive number that is equal to the sum of its proper divisors.

### Example

6 is a perfect number since its divisors are 1,2,3 and _1 + 2 + 3 = 6_.

<!-- more -->
### Screenshot

![Perfect Number Output](/images/perfect_number_output.jpg)

### Code

```csharp
using System;

namespace TestApplication
{
    class PerfectNumber
    {
        public static void Main()
        {
            int numbers = 1000;
            int sum;
            for(int i = 1;i <= numbers; i++)
            {
                sum = 0;
                for (int j = 1; j < i; j++)
                {
                    if (i % j == 0)
                    {
                        sum = sum + j;
                    }
                }
                if (i == sum)
                {
                    Console.WriteLine(i);
                }
            }
            Console.ReadLine();
        }
    }
}
```
