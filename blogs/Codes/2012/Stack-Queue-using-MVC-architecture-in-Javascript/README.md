---
title: Stack & Queue using MVC architecture in Javascript
date: 2012-06-18 08:54:07
categories:
  - Codes
tags:
  - code-example
  - javascript
  - mvc
redirectFrom: /2012/06/18/Stack-Queue-using-MVC-architecture-in-Javascript/
---
### Summary

MVC i.e. **Model-View-Controller** architecture usually separates the program in three parts which are **Model** i.e. the code we write in the form of modules or that represents a model, **View** that helps in displaying data to the user in the form of GUI and **Controller** usually handles the user gestures carried out on the GUI.

When using MVC architecture, our view can remain the same and by changing the Model it reflects the view with the help of the controller i.e. user interaction.

<!-- more -->
The following program represents one view for displaying the data and two models of a Stack & Queue Data Structures to impact on the control passed by the user i.e. GUI interaction.

Since we are using Javascript for representing these data structures, all functions act on the client side and no server side call is being made.


### Live Demo

<div>
<p data-height="505" data-theme-id="0" data-slug-hash="kkpNyz" data-default-tab="result" data-user="rsakhale" data-embed-version="2" class="codepen">See the Pen <a href="http://codepen.io/rsakhale/pen/kkpNyz/">MVC Datastructure JavaScript</a> by Rohan Sakhale (<a href="http://codepen.io/rsakhale">@rsakhale</a>) on <a href="http://codepen.io">CodePen</a>.</p>
<script async src="//assets.codepen.io/assets/embed/ei.js"></script>
</div>

### Screenshots

![MVC Architecture Data Structures Output #1](/images/output_mvc_stack_queue_1.jpg)
![MVC Architecture Data Structures Output #2](/images/output_mvc_stack_queue_2.jpg)
![MVC Architecture Data Structures Output #3](/images/output_mvc_stack_queue_3.jpg)
![MVC Architecture Data Structures Output #4](/images/output_mvc_stack_queue_4.jpg)

### Code

```html
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Data Structures in Javascript {Object Functional Programming} - {Using MVC Pattern}</title>

        <style type="text/css">
            .stackcontrol{margin-top:20px;}
            .box{margin: 20px auto;text-align: center;width: 500px;height: 450px;border-radius: 20px;box-shadow: 0 0 10px #000;padding: 20px;}
            input[type=text]{padding: 10px;border-radius: 10px;border: 0;box-shadow: 0 0 5px #000;}
            input[type=button]{margin: 0 10px 0 10px;width: 75px;padding: 10px;border-radius: 20px;box-shadow: 0 0 10px #000;border:0;cursor: pointer;}
            input:hover{box-shadow: 0 0 5px #b1b1b1;}
            p{background: darkseagreen;width:98%;margin:0;border: 2px dotted #000;}
            #stackview{width: 80%;height: 55%;margin: 5%;padding: 5%;background: #eaeaea;border-radius: 10px;-webkit-transition: all 3s ease-out;-moz-transition: all 3s ease-out;-o-transition: all 3s ease-out;transition: all 3s ease-out;}
            #stackmsg{margin: 10px;}
            .error{color: #ff0000;}.success{color: #00ff00;}
        </style>
    </head>
    <body>
        <div class="box">
            <div id="stackview"></div>
            <div class="stackcontrol">
                <select id="option"><option value="Stack">Stack</option><option value="Queue">Queue</option></select>
                <input type="button" id="push" value="push"/>
                <input placeholder="Enter number here" type="text" id="input" />
                <input type="button" id="pop" value="pop"/></div>
            <div id="stackmsg"></div>
            <a href="http://rohansakahle.com" title="Best site for finding the perfect IT solution">www.rohansakhale.com</a>
        </div>
        <script type="text/javascript">
            try{
                // Variables
                var m = document.getElementById("stackmsg");
                var stackview = document.getElementById("stackview");            
                var options = document.getElementById("option");
                var valueHandle = document.getElementById("input");
            
                function Stack(){
                    var sp = 10;
                    var stk = new Array(10);
                    this.push = function(v){
                        if(sp == 0){
                            this.print("error","Stack is Full");
                        }else{
                            stk[--sp] = v;
                            this.print("push","Success",v);
                        }
                    };
                    this.pop = function(){
                        if(sp == 10){
                            this.print("error","Stack is Empty");
                        }else{
                            var v = stk[sp++];
                            this.print("pop","Success",v);
                        }
                    };
                    this.print = function(type,msg,value){
                        if(type == "error"){
                            m.className = "error";
                        }else if(type == "push"){
                            var sp = document.createElement("p");
                            var txtNode = document.createTextNode(value);
                            sp.appendChild(txtNode);
                            stackview.insertBefore(sp, stackview.childNodes[0]);
                            m.className = "success";
                        }else if(type == "pop"){
                            stackview.removeChild(stackview.childNodes[0]);
                            m.className = "success";
                        }
                        m.innerHTML = msg;
                    };
                };
                function Queue(){
                    var queArray = new Array();
                    this.enqueue = function(v){
                        if(queArray.length == 10){
                            this.print("error","Queue is Full");
                        }else{
                            queArray.push(v);
                            this.print("enqueue","Successfully Added",v);
                        }
                    };
                    this.dequeue = function(){
                        if(queArray.length == 0){
                            this.print("error","Queue is Empty");
                        }else{
                            queArray.shift();
                            this.print("dequeue","Successfully Added");
                        }
                    };
                    this.print = function(type,msg,value){
                        if(type == "error"){
                            m.className = "error";
                        }else if(type == "enqueue"){
                            var sp = document.createElement("p");
                            var txtNode = document.createTextNode(value);
                            sp.appendChild(txtNode);
                            stackview.appendChild(sp);
                            m.className = "success";
                        }else if(type == "dequeue"){
                            stackview.removeChild(stackview.firstChild);
                            m.className = "success";
                        }
                        m.innerHTML = msg;
                    };
                };
            
                // Stack Object
                var s = new Stack();
                // Queue Object
                var e = new Queue();
            
                // Stack Execute
                function sExecute(){
                    var pushHandle = document.getElementById("push");
                    var popHandle = document.getElementById("pop");
                    pushHandle.value = "Push";
                    popHandle.value = "Pop";
                
                    pushHandle.onclick = function(){
                        var val = valueHandle.value;
                        if(val != ""){
                            s.push(val);
                        }else{
                        
                            m.innerHTML = "Enter valid value to push";
                            m.className = "error";
                        }
                        valueHandle.value = "";
                    };
                    popHandle.onclick = function(){
                        s.pop();
                    };
                };
            
                // Queue Execute
                function qExecute(){
                    var pushHandle = document.getElementById("push");
                    var popHandle = document.getElementById("pop");
                    pushHandle.value = "Enqueue";
                    popHandle.value = "Dequeue";
                
                    pushHandle.onclick = function(){
                        var val = valueHandle.value;
                        if(val != ""){
                            e.enqueue(val);
                        }else{
                            var m = document.getElementById("stackmsg");
                            m.innerHTML = "Enter valid value to enqueue";
                            m.className = "error";
                        }
                        valueHandle.value = "";
                    };
                    popHandle.onclick = function(){
                        e.dequeue();
                    };
                
                };
            
                // On change Function for Select Option
                function changeModel(){
                    var stackview = document.getElementById("stackview");
                    while(stackview.lastChild != null)
                        stackview.removeChild(stackview.lastChild);
                    if(options.value == "Stack"){sExecute();}else{qExecute();}
                
                }
                options.onchange = changeModel;
                window.onload = sExecute;
            }catch(e){alert(e);}
        </script>
    </body>
</html>
```
