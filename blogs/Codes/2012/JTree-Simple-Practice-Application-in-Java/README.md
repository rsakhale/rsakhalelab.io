---
title: JTree Simple Practice Application in Java
date: 2012-06-16 15:24:18
categories:
  - Codes
tags:
  - code-example
  - java
  - java-swing
redirectFrom: /2012/06/16/JTree-Simple-Practice-Application-in-Java/
---
### Summary

JTree is a control that displays a set of hierarchical data as an outline.

We will use it as a menu to navigate through various small applications we made previously.
<!-- more -->
The tree will basically contain each program as a node when selected using mouse, that particular program is displayed onto the JFrame.

Programs used in this code are already being posted before for practice, I will name the Class & the link to find the class

### Reference Code

1. [Simple Calculator in Java](/2012/06/16/Simple-Calculator-in-Java/)
1. [Factorial Program in Java](/2012/06/16/Factorial-Program-in-Java/)
1. [Calculate Celsius to Fahrenheit Temperature in Java](/2012/06/16/Calculate-Celsius-to-Fahrenheit-Temperature-in-Java/)
1. [Calculate Fahrenheit to Celsius Temperature in Java](/2012/06/16/Calculate-Fahrenheit-to-Celsius-Temperature-in-Java/)

### Screenshot

![JTree Simple Practice Application in Java Output 1](/images/output_jtree_prac_app_1.jpg)

![JTree Simple Practice Application in Java Output 2](/images/output_jtree_prac_app_2.jpg)

![JTree Simple Practice Application in Java Output 3](/images/output_jtree_prac_app_3.jpg)

![JTree Simple Practice Application in Java Output 4](/images/output_jtree_prac_app_4.jpg)

![JTree Simple Practice Application in Java Output 5](/images/output_jtree_prac_app_5.jpg)

### Code

```java
package com.rohansakhale.treedemo;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 *
 * @author RohanSakhale
 */
public class TreeDemo {

    static String lastComponent = "";

    public static void main(String[] args) {
        final JFrame jf = new JFrame("Practice Applications");

        DefaultMutableTreeNode pracApps = new DefaultMutableTreeNode("Practice Apps", true);
        DefaultMutableTreeNode calc = new DefaultMutableTreeNode("Calculator");
        DefaultMutableTreeNode fact = new DefaultMutableTreeNode("Factorial");
        DefaultMutableTreeNode temperature = new DefaultMutableTreeNode("Temperature", true);
        DefaultMutableTreeNode c2f = new DefaultMutableTreeNode("C to F");
        DefaultMutableTreeNode f2c = new DefaultMutableTreeNode("F to C");
        temperature.add(c2f);
        temperature.add(f2c);
        pracApps.add(calc);
        pracApps.add(fact);
        pracApps.add(temperature);
        final JTree tree = new JTree(pracApps);

        // JTree here can act as a menu for Practice Applications
        // We add it on the west side of the JFrame

        jf.add(BorderLayout.WEST, tree);

        /*
         * Classes created here within the same package can also be found online
         * Calc = http://rohansakhale.com/codes/39/simple-calculator-in-java
         * Factorial = http://rohansakhale.com/codes/40/factorial-program-in-java
         * C2F = http://rohansakhale.com/codes/41/calculate-celsius-to-fahrenheit-temperature-in-java
         * F2C = http://rohansakhale.com/codes/42/calculate-fahrenheit-to-celsius-temperature-in-java
         * 
         */
        final Calc calculatorPanel = new Calc();
        final Factorial factorialPanel = new Factorial();
        final C2F c2fObj = new C2F();
        final F2C f2cObj = new F2C();

        // Lets change the panel on center of JFrame
        // by checking the mouse click
        tree.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent me) {
                TreePath path = tree.getPathForLocation(me.getX(), me.getY());
                if (path != null) {
                    // Check the last added component and remove it
                    if (!lastComponent.equals("")) {
                        if (lastComponent.equals("Factorial")) {
                            jf.remove(factorialPanel);
                        } else if (lastComponent.equals("C to F")) {
                            jf.remove(c2fObj);
                        } else if (lastComponent.equals("Calculator")) {
                            jf.remove(calculatorPanel);
                        } else if (lastComponent.equals("F to C")) {
                            jf.remove(f2cObj);
                        }
                    }

                    // Add the new component on the frame
                    // Depending on the Selected Tree Node
                    if (path.toString().contains("Calculator")) {
                        jf.add(calculatorPanel);
                        lastComponent = "Calculator";
                    } else if (path.toString().contains("Factorial")) {
                        jf.add(factorialPanel);
                        lastComponent = "Factorial";
                    } else if (path.toString().contains("C to F")) {
                        jf.add(c2fObj);
                        lastComponent = "C to F";
                    } else if (path.toString().contains("F to C")) {
                        jf.add(f2cObj);
                        lastComponent = "F to C";
                    }
                    jf.repaint();
                }
            }
        });
        jf.setSize(600, 400);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setVisible(true);
    }
}
```
