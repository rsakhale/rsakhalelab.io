---
title: Funny Program
date: 2012-01-27 08:03:25
categories:
  - Codes
tags:
  - code-example
  - cpp
redirectFrom: /2012/01/27/Funny-Program/
---
### Summary

This is a funny time-pass program.

Give any input and check the output given there.
<!-- more -->
Idea taken from: mycfiles.com

### Code

```cpp
#include "stdio.h"
#include "conio.h"

void main()
{
	char ch[] = "I am an IDIOT.";
	char op = 'A';
	int i=0;
	clrscr();
	printf("Press '0' to exit. Enter what you feel:=> ");
	while(op != '0')
	{
		op = getch();
		printf("%c",ch[i++]);
		if(i == 14)
		{
			printf(" ");
			i = 0;
		}
	}
}
```
