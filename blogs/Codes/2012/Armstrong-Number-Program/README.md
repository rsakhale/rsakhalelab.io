---
title: Armstrong Number Program
date: 2012-01-27 08:03:45
categories:
  - Codes
tags:
  - code-example
  - cpp
redirectFrom: /2012/01/27/Armstrong-Number-Program/
---
### Summary

Armstrong numbers also known as narcissistic numbers.

Armstrong numbers are the sum of their own digits to the power of the number of digits.
<!-- more -->
```
153 = (1*1*1) + (5*5*5) + (3*3*3)
```

In this program will be find a 3 Digit number being entered by user is an Armstrong number or not.


For more details on Armstrong Number [visit here](https://en.wikipedia.org/wiki/Narcissistic_number).

### Screenshot

![Output Screen for Armstrong Number Program](/images/armstrong_output.jpg)

### Code

This program is hardcoded to 3 digits only

```cpp
#include "stdio.h"
#include "conio.h"

void main()
{
	int num;
	int x,y,z;
	// x resembles => Units Place
	// y resembles => Thousands Place
	// z resembles => Hundreds Place

	clrscr();

	printf("Enter the 3Digit number=> ");
	scanf("%d", &num); //store the value in num

	// Separated the number in terms of places

	x = num % 10;
	y = num / 100;
	z = (num / 10) % 10;

	// Cubing each place

	x = x * x * x;
	y = y * y * y;
	z = z * z * z;

	if(num == (x+y+z))
	{
		printf("The entered number is an Armstrong Number");
	}
	else
	{
		printf("It is not an Armstrong Number");
	}
	getch();
}
```
