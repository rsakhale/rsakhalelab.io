---
title: Pattern Printing 4
date: 2012-03-28 14:38:41
categories:
  - Codes
tags:
  - code-example
  - csharp
  - pattern-printing
redirectFrom: /2012/03/28/Pattern-Printing-4/
---
### Summary

The following code prints in the form of pyramid as shown below.

<!-- more -->
```
   *
  ***
 *****
*******
 *****
  ***
   *
```

### Screenshot

![Patter Printing 4 Screenshot](/images/pattern4_output.jpg)

### Code

```java
package com.rohansakhale.quicktest;

/**
 *
 * @author Rohan Sakhale
 */
public class Pattern5 {

    public static void main(String[] args) {
        int i, j, k = 0;
        for (i = 1; i <=4; i++) {
            for (j = 4; j > i; j--) {
                System.out.print(" ");
            }
            for (k = 0; k < (2 * i - 1); k++) {
                System.out.print("*");
            }
            System.out.println(");
        }
        for (i = (k - i + 1); i > 0; i--) {
            for (j = 4; j > i; j--) {
                System.out.print(" ");
            }
            for(k = 0; k < (2 * i - 1);k++){
                System.out.print("*");
            }
            System.out.println(");
        }
    }
}
```
