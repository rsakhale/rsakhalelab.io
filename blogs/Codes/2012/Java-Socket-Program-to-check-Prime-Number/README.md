---
title: Java Socket Program to check Prime Number
date: 2012-02-13 15:14:51
categories:
  - Codes
tags:
  - code-example
  - java
  - socket
redirectFrom: /2012/02/13/Java-Socket-Program-to-check-Prime-Number/
---
### Summary

This is a [socket program](https://en.wikipedia.org/wiki/Network_socket), it consists of Server side program & client side program.
<!-- more -->
_Server will provide web service for validating the number is prime or not and returns true or false._

_Client sends the integer number to the server & gets the respond accordingly._

### Steps

1. Compile both classes
1. Run server class first
1. Run client class that communicates with Server

> This will work only for single input, inorder to make it work for infinite input as per user, simple changes have to be made in both the codes.

### Code

#### Server Code: ServerPrime.java

```java

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rohansakhale;


/**
 *
 * @author Rohan Sakhale
 */

import java.io.*;
import java.net.*;

public class ServerPrime {

    public static boolean isPrime(int number){
        boolean isPrimeNum = false;
        int i = (int) Math.ceil(Math.sqrt(number));
        while(i>1)
        {
            if((number != i) && (number % i ==0))
            {
                isPrimeNum = false;
                break;
            }
            else if(!isPrimeNum)
            {
                isPrimeNum = true;
            }
            --i;
        }
        return isPrimeNum;
    }
    public static void main(String [] args) throws Exception
    {
        Socket s;
        int port = 9000;
        ServerSocket ss = new ServerSocket(port);
        System.out.println("Waiting for client");
        s = ss.accept();
        BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
        PrintWriter pw = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));
        int num = Integer.parseInt(br.readLine());
        System.out.println("Number sent by client: " + num);
        pw.println(ServerPrime.isPrime(num));
        pw.flush();
    }
}
```

#### Client Code: "ClientPrime.java"

```java
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rohansakhale;

/**
 *
 * @author Rohan Sakhale
 */

import java.io.*;
import java.net.*;

public class ClientPrime {
    public static void main(String [] args) throws Exception
    {
        int port = 9000;
        Socket s;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        s = new Socket(InetAddress.getLocalHost(),port);
        PrintWriter pw = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));
        BufferedReader brl = new BufferedReader(new InputStreamReader(s.getInputStream()));
        System.out.print("Enter any number: ");
        String str = br.readLine();
        pw.println(str);
        pw.flush();
        String msg = brl.readLine();
        if(msg.equals("true"))
        {
            System.out.println("It is a prime number");
        }
        else
        {
            System.out.println("It is not a prime number");
        }
    }
}
```
