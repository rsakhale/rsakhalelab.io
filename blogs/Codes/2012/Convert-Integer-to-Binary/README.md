---
title: Convert Integer to Binary
date: 2012-02-12 23:56:09
categories:
  - Codes
tags:
  - code-example
  - java
redirectFrom: /2012/02/12/Convert-Integer-to-Binary/
---
### Summary

This program converts Integer number received from user and displays the binary form of that integer number onto the screen.
<!-- more -->
_This program explains the logic, but Java also provides in-built methods in java.lang package, which can be directly used in bigger projects._

### Example

**Integer class provides toBinaryString(int value) static method.**

### Code

```java
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package conversionprogram;

/**
 *
 * @author Rohan Sakhale
 */
public class ConversionProgram {

    /**
     * @param args the command line arguments
     */
    public static String toBinary(int num)
    {
        String result = "";
        int base = 2; // 2 states Binary Conversion
        
        // Iterate untill num is more than 0
        // On every iterate divide num by the base
        // The residue we get will be adding before our result
        while(num>0)
        {
            int residue = num % base;
            result = residue + result;
            num = num / base;
        }
        return result; // Returns the final binary string
    }

    public static void main(String[] args) {
        System.out.println("Our Binary Output: " + toBinary(102));
        System.out.println("Inbuilt Java Binary Output: " + Integer.toBinaryString(102));
    }
}
```
