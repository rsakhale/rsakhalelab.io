---
title: Random Panel Color in Java
date: 2012-06-16 19:27:21
categories:
  - Codes
tags:
  - code-example
  - java
  - java-swing
redirectFrom: /2012/06/16/Random-Panel-Color-in-Java/
---
### Summary

Colors in computers are generally made up using RGB mode i.e. Red, Green, Blue.

So in java if we generate random color values for RGB from 0 to 255 each we can make a whole new color out of it by passing this RGB to the color object.

<!-- more -->
To show the demo Swings have been used where the components are JFrame, JPanel & JButton placed using BorderLayout onto center & south.

The ActionListener for the button does the job of changing the background color of the panel.

### Screenshot

![Random Panel Color in Java screenshot](/images/output_random_color_panel.gif)

### Code

```java
package com.rohansakhale.randomcolorpaneldemo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Rohan Sakhale
 */
public class RandomColorPanelDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame jf = new JFrame("Random Color Demo by rohansakhale");
        JButton jb = new JButton("Change Color");
        final JPanel jp = new JPanel();
 
        jf.add(jp);
        jf.add(BorderLayout.SOUTH, jb);
 
        jb.addActionListener(new ActionListener() {
 
            @Override
            public void actionPerformed(ActionEvent e) {
                int red, green, blue;
                red = (int) (Math.random() * 255);
                green = (int) (Math.random() * 255);
                blue = (int) (Math.random() * 255);
                Color c = new Color(red, green, blue);
                jp.setBackground(c);
            }
        });
 
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setSize(400, 300);
        jf.setVisible(true);
    }
}
```
