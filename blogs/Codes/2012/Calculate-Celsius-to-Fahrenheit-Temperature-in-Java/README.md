---
title: Calculate Celsius to Fahrenheit Temperature in Java
date: 2012-06-16 15:31:13
categories:
  - Codes
tags:
  - code-example
  - java
  - java-swing
redirectFrom: /2012/06/16/Calculate-Celsius-to-Fahrenheit-Temperature-in-Java/
---
### Summary
In-order to convert from Celsius to Fahrenheit, we have to apply a simple logic onto Celsius "Multiply by 9, then divide by 5, then add 32".

Our code provides a simple GUI, where the user has to enter the temperature in Celsius and after clicking on convert displays the Fahrenheit temperature below button in the form of JLabel.
<!-- more -->
The Class has been designed by extending JPanel which can be called from other classes and added onto the JFrame for displaying.

### Screenshot

![Calculate Celsius to Fahrenheit Temperature in Java Screenshot](/images/output_celsius_fahrenheit_java.jpg)

### Code

```java
package com.rohansakhale.treedemo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author RohanSakhale
 */
public class C2F extends JPanel {
    
    JTextField tfInputTemp;
    JLabel lInputTemp, lAnswer;
    JButton bCalc;
    JPanel InputTemp, collections;
    
    public C2F() {
        tfInputTemp = new JTextField(15);
        lInputTemp = new JLabel("Enter Temperature in Celsius: ");
        lAnswer = new JLabel("Answer: ");
        bCalc = new JButton("Convert");
        InputTemp = new JPanel();
        collections = new JPanel();
        collections.setLayout(new BoxLayout(collections, BoxLayout.Y_AXIS));
        InputTemp.add(lInputTemp);
        InputTemp.add(tfInputTemp);
        collections.add(InputTemp);
        collections.add(bCalc);
        collections.add(lAnswer);
        bCalc.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!tfInputTemp.getText().equals("")) {
                    double temp = Double.parseDouble(tfInputTemp.getText());
                    double fahrenheit = ((temp * 9) / 5) + 32;
                    lAnswer.setText("Answer: " + fahrenheit + " F");
                }
            }
        });
        add(collections);
    }
}

class C2FMainDemo{
    public static void main(String[] args) {
        JFrame c2fFrame = new JFrame("Celsius to Fahrenheit Demo Program");
        C2F c2f = new C2F();
        c2fFrame.add(c2f);
        c2fFrame.setSize(400, 150);
        c2fFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        c2fFrame.setVisible(true);
    }
}
```
