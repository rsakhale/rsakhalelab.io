---
title: CSS Media Queries for Responsive Web Development
date: 2017-01-31 13:07:12
categories:
  - Codes
tags:
  - css
  - code-example
  - media-queries
redirectionFrom: /2017/01/31/CSS-Media-Queries-for-Responsive-Development/
---
# Information

One of the main factor for responsive website development is developing css for multiple screen devices. Often it becomes difficult when we have so many set of devices and different set of dimension to each of those device, what dimensions we should fit. There are many answers already available on internet via css-tricks, stackoverflow etc.
<!-- more -->
If you are looking for quite a standard and less complex solution, below listed media queries are best fit, as we have been using them for quite a good projects and have achieved decent solution.

# Code

```css
/* smartphones, iPhone, portrait phones width 320 */
@media (max-width:320px)  {
	
}

/* smartphones, iPhone, portrait phones width 384 */
@media (min-width:321px) and (max-width:384px) {
	
}

/* smartphones, iPhone, portrait phones width 480 */
@media (min-width:385px) and (max-width:480px) {
	
}

/* portrait e-readers (Nook/Kindle), smaller tablets @ 600 or @ 640 wide. */
@media (min-width:481px) and (max-width: 640px) {

}

/* portrait tablets, portrait iPad, landscape e-readers, landscape 800x480 or 854x480 phones */
@media (min-width:641px) and (max-width: 960px)  {
	
}

/* tablet, landscape iPad, lo-res laptops ands desktops */
@media (min-width:961px) and (max-width: 1024px) { 

}

/* big landscape tablets, laptops, and desktops */
@media (min-width:1025px) and (max-width: 1360px) {

}

/* hi-res laptops and desktops */
@media (min-width: 1361px) {

}
```
# More Sources

But if you are looking for every device solution, this [article](https://css-tricks.com/snippets/css/media-queries-for-standard-devices/) from **css-tricks** is perfect solution.

If you are still confused and looking to explore more comprehensive solution, try this [repository](http://cssmediaqueries.com/).
