---
title: About Rohan Sakhale
date: 2024-01-03 12:11:10
---
> "Given the right set of raw materials, it can be developed into a finished saleable product."

Renowned as a **Software Mentor** by passion and an Engineer by profession, I bring over 15+ years of Web Development experience as a Freelancer and 13+ years in the industry. My journey, richly influenced by [Prof. Rajesh Patkar](https://www.rajeshpatkar.com) (my mentor, my guru), has been a testament to developing raw ideas into efficient, effective, and market-ready products.

When I'm not deeply immersed in coding, I dedicate my time to mentoring students, sharing practical knowledge on the latest tech trends and coding practices.

My career path includes significant roles in top Fortune 500 companies like [Saba Software](https://saba.com) acquired by [Cornerstone OnDemand](https://www.cornerstoneondemand.com/) and [Diebold Nixdorf](https://dieboldnixdorf.com), where I received numerous accolades for my contributions. 

Following this, I played a pivotal role at [Indigital Technologies LLP](https://indigitalit.com), contributing to innovative patent-pending products poised to disrupt global markets.

From 2021 to November 2023, I expanded my horizons with [Thunderbite](https://thunderbite.com) acquired by [XtremePush](https://xtremepush.com/), a Gibraltar-based company, where I honed my skills further. Currently, I am excited to be a part of [OnePin Inc](https://onepin.com), a dynamic organization in the US, working remotely and pushing the boundaries of technology.

In parallel, I continue nurturing my startup, "[Sai Ashirwad Informatia](https://saiashirwad.com)", providing client services, infrastructure support, and training.

### My technological expertise has evolved to include:

1. Certified [Laravel](https://laravel.com), [Livewire](https://livewire.laravel.com), [Alpine.js](https://alpinejs.dev)
1. [Python](https://www.python.org) ([AWS Chalice](https://aws.github.io/chalice/), [Django](https://www.djangoproject.com/))
1. [React.js](https://react.dev/)
1. [AWS Serverless](https://aws.amazon.com/serverless/)


In addition to these, I maintain my proficiency in [#PHP](/tag/php), [#Java](/tag/java), [#JavaScript](/tag/javascript), #Vue, #CI, #GitLab, #ProjectManagement, #TestAutomation, #AndroidExpert, #Flutter, #SonarQube, and #CodeQuality.

As I continue my journey in 2024, I remain committed to innovation, quality, and sharing knowledge, continuously striving to transform the digital landscape.
